import { defaults, extend } from 'underscore';
import Builder from './builder.js';
import defaultResources from './assets/resources.js';

export default class Api {
  constructor(resources, omitEmptyParams) {
    this.config = {};
    this.variables = {};
    this.builder = null;
    this.omitEmptyParams = omitEmptyParams;
    this.init(resources);
  }

  // Создает объект API
  init(resources) {
    this.resources = resources || {};
    defaults(this.resources, defaultResources);
    this.created(resources);
  }

  // Установить значение по умолчанию
  default(key, val) {
    if (typeof val !== 'undefined') {
      this.variables[key] = val;
    }
    return this.variables[key];
  }

  // Установить конфигурацию ресурса
  define(name, resource) {
    this.resources[name] = resource;
    return this;
  }

  // Дополнить конфигурацию ресурса
  extend(name, resource) {
    extend(this.resources[name], resource);
    return this;
  }

  getConfig(name) {
    const config = this.resources[name] || {};
    const copy = Object.assign({}, config);
    return defaults(copy, this.resources.default);
  }

  uri(uri, name, isPublic) {
    this.builder = new Builder(this.variables, this.omitEmptyParams);
    this.config = this.getConfig(name);
    this.builder.setApi(this.config, isPublic);
    const resource = this.builder.uri(uri);
    this.builder.setResource(this.getConfig(resource), resource);
    this.initialized(this.config);

    return this.builder.req();
  }

  res(resource, name, isPublic) {
    this.builder = new Builder(this.variables, this.omitEmptyParams);
    this.config = this.getConfig(name || resource);
    this.builder.setApi(this.config, isPublic);
    this.builder.setResource(this.getConfig(resource), resource);
    this.initialized(this.config);

    return this.builder.req();
  }

  setHandlers(handlers) {
    this.handlers = handlers;
  }

  auth(key, name) {
    if (key) {
      if (name) {
        this.extend(name, { key });
      } else {
        this.default('key', key);
      }
    }
    return this;
  }

  public() {
    this.builder.public();
    return this.builder;
  }

  private() {
    this.builder.private(this.config);
    return this.builder;
  }

  status() {
    return this.builder?.requests?.handler?.status;
  }

  // Хук создания API
  created() {} // eslint-disable-line
  // Хук инициализации Ресурса
  initialized() {} // eslint-disable-line

}
