import { extend } from 'underscore';
import routes from './assets/routes.js';

export default class Router {
  constructor() {
    this.root = '/';
    this.url = '';
    this.routing = routes;
    this.reset();
  }

  static slash(host) {
    if (!host || typeof host !== 'string') {
      return '/';
    }
    const last = host.slice(-1);
    if (last === '/') {
      return host;
    }
    return `${host}/`;
  }

  reset(host, prefix, version, postfix) {
    this.host = host;
    this.prefix = prefix;
    this.version = version;
    this.postfix = postfix;
    this.subresource = null;
    this.subId = null;
    this.params = {};
  }

  init(host, prefix, version, postfix) {
    this.reset(host, prefix, version, postfix);
    const ver = version ? `${version}/` : '';
    const pre = prefix ? `${prefix}/` : '';
    const post = postfix ? `${postfix}/` : '';
    this.root = `${Router.slash(host)}${pre}${ver}${post}`;

    return this;
  }

  setRouting(routing, resource) {
    this.routing = {
      route: resource,
    };
    extend(this.routing, routes, routing);
    return this;
  }

  extendParams(params) {
    this.params = Object.assign({}, params, this.params);
    return this.params;
  }

  setParams(params, url) {
    const stack = this.extendParams(params);
    const result = url.replace(/\{(.*?)\}/gi, (match, token) => {
      const slug = stack[token];
      delete stack[token];
      return slug;
    });
    if (stack && stack.subId) {
      this.setSubId(stack.subId);
      delete stack.subId;
    }
    // console.log('url: ', [this.root, result, params]);
    this.params = stack || {};
    return result;
  }

  setSubId(subId) {
    if (subId) {
      this.subId = subId;
    }
  }

  setSubResource(path, subId) {
    this.setSubId(subId);
    this.subresource = path;
  }

  getUrlPath(method) {
    let result;
    const { route } = this.routing;
    const action = this.routing[method];
    result = route || '';
    if (result && action) {
      result = `${result}/${action}`;
    } else if (action) {
      result = action;
    }
    if (this.subresource) {
      result = `${result}/${this.subresource}`;
      if (this.subId) {
        result = `${result}/${this.subId}`;
      }
    }
    return result;
  }

  setUrl(method, params, url) {
    let result;
    let { root } = this;
    if (this.isAbsolute(url)) {
      root = '';
      result = url;
    } else if (url) {
      result = url;
    } else {
      result = this.getUrlPath(method);
    }
    result = this.setParams(params, result);
    this.url = root + result;
    return this.url;
  }

  // eslint-disable-next-line
  isAbsolute(url) {
    const re = new RegExp('(www|://)');
    return re.test(url);
  }
}
