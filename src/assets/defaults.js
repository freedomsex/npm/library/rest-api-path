
import routes from './routes.js';
import config from './config.js';

config.routing = routes;

export default config;
