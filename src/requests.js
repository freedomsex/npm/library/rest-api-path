import { mapObject, isObject, isEmpty, pick, isNull, isNaN, isUndefined } from 'underscore';

export default class requests {
  constructor(omitEmpty) {
    this.omitEmpty = (omitEmpty === true);
    this.options = {};
    this.router = null;
    this.handler = null;
    this.wait = 0;
  }

  setOptions(data) {
    Object.assign(this.options, data);
    return this;
  }

  get(params, url, wait) {
    return this.request('get', 'get', null, params, url, wait);
  }

  load(params, url, wait) {
    return this.request('get', 'load', null, params, url, wait);
  }

  cget(params, url, wait) {
    return this.request('get', 'cget', null, params, url, wait);
  }

  send(params, url, wait) {
    return this.request('get', 'send', null, params, url, wait);
  }

  post(data, params, url, wait) {
    return this.request('post', 'post', data, params, url, wait);
  }

  save(data, params, url, wait) {
    return this.request('post', 'save', data, params, url, wait);
  }

  new(data, params, url, wait) {
    return this.request('post', 'new', data, params, url, wait);
  }

  edit(data, params, url, wait) {
    return this.request('post', 'edit', data, params, url, wait);
  }

  remove(data, params, url, wait) {
    return this.request('post', 'remove', data, params, url, wait);
  }

  delete(params, url, wait) {
    return this.request('delete', 'delete', null, params, url, wait);
  }

  put(data, params, url, wait) {
    return this.request('put', 'put', data, params, url, wait);
  }

  patch(data, params, url, wait) {
    return this.request('patch', 'patch', data || {}, params, url, wait);
  }

  option(params, url, wait) {
    return this.request('option', 'option', null, params, url, wait);
  }

  // axiosData(data, method) {
  //   return data || ['post', 'put', 'patch'].includes(method);
  // }

  adaptParams(params) {
    if (!this.omitEmpty) {
      return params;
    }
    function prune(object) {
      const result = mapObject(object, (value) => {
        if (isObject(value)) {
          if (isEmpty(value)) {
            return null;
          }
          return prune(value);
        }
        return value;
      });
      return pick(result, value => value !== '' && !isNull(value) && !isNaN(value) && !isUndefined(value));
    }
    return prune(params);
  }

  setRouter(router) {
    this.router = router;
    return this;
  }

  request(method, action, data, params, url, wait) {
    this.beforeRequest({ method, action, data, params, url, wait });
    if (!this.router) {
      throw new Error('Router instance not found');
    }
    const uri = this.router.setUrl(action, params, url);
    let seconds = wait || this.wait || 0;
    if (seconds < this.wait) {
      seconds = this.wait;
    }

    return this.handle({ method, uri, data, params: this.router.params, seconds });
  }

  setHandler(handler) {
    if (!handler) {
      return this;
    }
    this.handler = handler;

    return this;
  }

  handle(config) {
    this.beforeHandle(config);
    if (this.handler) {
      this.handler.setOptions(this.options);
      return this.handler.request(config);
    }
    return config;
  }

  beforeRequest() {} // eslint-disable-line

  beforeHandle() {} // eslint-disable-line
}
