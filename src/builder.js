import { defaults, extend } from 'underscore';
import Router from './router.js';
import Requests from './requests.js';

export default class Builder {
  constructor(variables, omitEmptyParams) {
    this.name = 'default';
    this.config = {};
    this.router = new Router();
    this.requests = new Requests(omitEmptyParams);
    this.requests.setRouter(this.router);
    this.variables = Object.assign({}, variables);
  }

  setApi(config, isPublic) {
    this.router.init(
      config.host,
      config.prefix,
      config.version,
      config.postfix,
    );
    this.setDelay(config.delay);
    if (!this.isAuth() && !isPublic && config.authorized !== false) {
      this.auth(config.key);
    }
    this.requests.setOptions(this.config);
    return this;
  }

  setResource(config, name) {
    if (!name && !config.name) {
      throw new Error('You need specify `name` or `config.name`');
    }
    this.name = name || config.name;
    this.router.setRouting(config.routing || {}, this.name);

    return this;
  }

  uri(uri) {
    let parts = uri.split('/');
    if (!parts[0]) {
      parts = parts.splice(1);
    }
    if (this.router.prefix) {
      if (parts[0] === this.router.prefix) {
        parts = parts.splice(1);
      } else {
        throw Error('The URI must contain the prefix');
      }
    }
    if (this.router.version) {
      if (parts[0] === this.router.version) {
        parts = parts.splice(1);
      } else {
        throw Error('The URI must contain the version');
      }
    }
    if (this.router.postfix) {
      if (parts[0] === this.router.postfix) {
        parts = parts.splice(1);
      } else {
        throw Error('The URI must contain the postfix');
      }
    }
    let resource = null;
    let id = null;
    if (parts.length >= 1) {
      [id] = parts.splice(-1);
    } else {
      throw Error('The URI must contain the ID');
    }
    if (parts.length >= 1) {
      resource = parts.join('/');
    } else {
      throw Error('The URI must contain the resource');
    }
    this.router.extendParams({ id });

    return resource;
  }

  req(config, name, isPublic) {
    if (config) {
      this.setApi(config, isPublic);
      this.setResource(config, name);
    }
    return this.requests;
  }

  sub(path, subId) {
    this.router.setSubResource(path, subId);
    return this;
  }

  setDelay(sec) {
    this.router.wait = sec || 0;
    return this;
  }

  setHeaders(headers) {
    if (headers) {
      defaults(this.config, { headers: {} });
      extend(this.config.headers, headers);
    }
    return this;
  }

  setBaseURL(url) {
    const base = url || this.variables.baseURL || null;
    if (base) {
      this.config.baseURL = base;
    }
    return this;
  }

  isAuth() {
    return this.config.headers && this.config.headers.Authorization;
  }

  auth(key) {
    const authKey = key || this.variables.key;
    if (authKey) {
      this.setHeaders({ Authorization: `Bearer ${authKey}` });
    }
    return this;
  }

  public() {
    if (this.isAuth()) {
      delete this.config.headers.Authorization;
    }
    return this;
  }

  private(config) {
    if (!this.isAuth()) {
      this.auth(config.key);
    }
    return this;
  }

  baseURL() {
    return this.router.root;
  }

  lastURL() {
    return this.router.url;
  }

  setConfig(config) {
    extend(this.config, config);
    return this;
  }

  clear() {
    this.lastConfig = Object.assign({}, this.config);
    this.config = {};
    return this;
  }

  getConfig() {
    return this.config;
  }

  lastConfig() {
    return this.lastConfig;
  }
}
