module.exports = {
  root: true,
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
  },
  extends: [
    'webpack',
    'eslint:recommended',
    'plugin:import/recommended',
  ],
  env: {
    node: true,
    es6: true,
  },
  rules: {
    'import/extensions': ['error', 'ignorePackages'],
  },
  ignorePatterns: ['/*', '!/src'],
};
