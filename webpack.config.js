import ESLintPlugin from 'eslint-webpack-plugin';

export default {
  externals: {
    underscore: 'underscore',
  },
  plugins: [
    new ESLintPlugin({
      extensions: ['.ts', '.js'],
      exclude: 'node_modules',
    }),
  ],
};
