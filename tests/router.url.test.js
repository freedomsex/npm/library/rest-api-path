import Router from '../src/router.js';

describe('Создание ресурсов', () => {
  let router;
  beforeEach(() => {
    router = new Router();
  });

  it('получить простой ресурс', async () => {
    router.init('somehost', 'api', 'v1');
    const url = router.setUrl('get', { id: 123 }, null);
    expect(url).toBe('somehost/api/v1/123');
  });
  it('получить внешний ресурс', async () => {
    router.init(null, 'api', 'v1');
    const url = router.setUrl('get', { id: 123 }, 'https://www.google.com/{id}');
    expect(url).toBe('https://www.google.com/123');
  });
  it('получить параметры', async () => {
    router.init('somehost', 'api', 'v1');
    router.setUrl('get', { id: 123, str: 'srting' });
    expect(router.params).toEqual({ str: 'srting' });
  });
  it('получить путь', async () => {
    router.init('somehost', 'api', 'v1');
    router.setUrl('get', { id: 123, str: 'srting' });
    expect(router.url).toBe('somehost/api/v1/123');
  });
  it('установить параметры', async () => {
    router.init('somehost', 'api', 'v1');
    const url = router.setParams({ id: 123, str: 'srting' }, 'users/{id}');
    expect(url).toBe('users/123');
    expect(router.params).toEqual({ str: 'srting' });
  });
});
