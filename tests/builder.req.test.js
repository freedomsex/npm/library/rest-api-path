import Builder from '../src/builder.js';

describe('Проверить билдер запросов', () => {
  let builder;
  beforeEach(() => {
    builder = new Builder();
  });

  it('get', async () => {
    // builder = new Builder({});
    builder.setApi({
      host: 'http://somehost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    });
    builder.setResource({
      routing: null,
    }, 'users');

    const result = builder.req().get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: 'http://somehost/api/v1/users/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
  it('req params', async () => {
    builder = new Builder();
    const result = builder.req({
      host: 'http://otherhost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    }, 'files').get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/files/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
  it('req params name', async () => {
    builder = new Builder();
    const result = builder.req({
      name: 'products',
      host: 'http://otherhost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    }).get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/products/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
  it('req params name priority', async () => {
    builder = new Builder();
    const result = builder.req({
      name: 'products',
      host: 'http://otherhost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    }, 'files').get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/files/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
  it('req uri', async () => {
    builder = new Builder();
    let config = {
      name: 'files',
      host: 'http://otherhost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    };

    let uri = '/api/v1/files/123';
    builder.setApi(config);
    builder.uri(uri, 'files');
    let resource = builder.uri(uri);
    builder.setResource(config, resource);

    const result = builder.req().get();
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/files/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
});
