import Router from '../src/router.js';
import Requests from '../src/requests.js';

describe('Создание ресурсов', () => {
  let router;
  let requests;
  beforeEach(() => {
    router = new Router();
    requests = new Requests();
  });

  it('get', async () => {
    router.init('somehost', 'api', 'v1');
    requests.setRouter(router);
    const result = requests.get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: 'somehost/api/v1/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });

  it('post', async () => {
    router.init('http://somehost', 'api', 'v1');
    requests.setRouter(router);
    const result = requests.post({ page: 2 }, { id: 123, str: 'srting' });
    expect(result).toEqual({
      method: 'post',
      uri: 'http://somehost/api/v1/',
      data: { page: 2 },
      params: { id: 123, str: 'srting' },
      seconds: 0,
    });
  });
  it('post params', async () => {
    router.init('http://somehost', 'api', 'v1');
    requests.setRouter(router);
    const result = requests.post({ page: 2 }, { id: 123, str: 'srting' }, 'users/{id}');
    expect(result).toEqual({
      method: 'post',
      uri: 'http://somehost/api/v1/users/123',
      data: { page: 2 },
      params: { str: 'srting' },
      seconds: 0,
    });
  });
  it('post wait', async () => {
    router.init('http://somehost', 'api', 'v1');
    requests.setRouter(router);
    const result = requests.post({ page: 2 }, { id: 123, str: 'srting' }, 'users/{id}', 3);
    expect(result).toEqual({
      method: 'post',
      uri: 'http://somehost/api/v1/users/123',
      data: { page: 2 },
      params: { str: 'srting' },
      seconds: 3,
    });
  });
});
