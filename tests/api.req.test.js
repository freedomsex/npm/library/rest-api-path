import Api from '../src/index.js';

describe('Проверить билдер', () => {
  let api;
  beforeEach(() => {
    api = new Api();
  });

  it('get', async () => {
    const result = api.res('users').get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: '/users/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });

  it('empty res', async () => {
    api = new Api({});
    const result = api.res('users').get({ id: 123 });
    expect(result).toEqual({
      method: 'get',
      uri: '/users/123',
      params: {},
      data: null,
      seconds: 0,
    });
  });
  it('get root', async () => {
    api = new Api({});
    const result = api.res('users').router.root;
    expect(result).toEqual('/');
  });
  it('get root undefined', async () => {
    api = new Api(undefined);
    const result = api.res('users').router.root;
    expect(result).toEqual('/');
  });
  it('get root null', async () => {
    api = new Api(null);
    const result = api.res('users').router.root;
    expect(result).toEqual('/');
  });
  it('resources default', async () => {
    api = new Api({
      default: {
        host: 'http://localhost',
    }});
    const result = api.res('users').router.root;
    expect(result).toEqual('http://localhost/');
  });
  it('host slash', async () => {
    api = new Api({
      default: {
        host: 'http://localhost/',
    }});
    const result = api.res('users').router.root;
    expect(result).toEqual('http://localhost/');
  });
  it('auth', async () => {
    api.auth('secret_key')
    api.res('users')
    expect(api.builder.config.headers).toEqual({ Authorization: 'Bearer secret_key' });
  });
  it('auth resource', async () => {
    api = new Api({
      default: {
        host: 'http://localhost/',
    }});
    api.auth('secret_key','default')
    api.res('users','default')
    expect(api.builder.config.headers).toEqual({ Authorization: 'Bearer secret_key' });
  });
  it('req auth options', async () => {
    api.auth('secret_key')
    const req = api.res('users')
    console.log(req.options);
    expect(req.options.headers).toEqual({ Authorization: 'Bearer secret_key' });
  });
});
