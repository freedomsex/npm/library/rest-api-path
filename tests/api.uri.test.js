import Api from '../src/index.js';

describe('Проверить запросы по URI', () => {
  let api;
  beforeEach(() => {
    api = new Api();
  });

  it('get', async () => {
    api.define('files', {
      name: 'users',
      host: 'http://otherhost',
      prefix: 'api',
      version: 'v1',
      postfix: '',
      delay: 0,
      key: null,
    });
    let result = api.uri('/api/v1/users/123', 'files').get();
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/users/123',
      params: {},
      data: null,
      seconds: 0,
    });
    result = api.uri('/api/v1/users/321', 'files').get();
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/users/321',
      params: {},
      data: null,
      seconds: 0,
    });
    result = api.res('users', 'files').get({ id: 789 });
    expect(result).toEqual({
      method: 'get',
      uri: 'http://otherhost/api/v1/users/789',
      params: {},
      data: null,
      seconds: 0,
    });
  });
});
